# -*- coding: utf-8 -*-
"""This script create alterts by comparison of a vegetation detection mask with the db"""

#
# Copyright (c) IGN 2019 -- 2021.
# This file belongs to subproject uc5a_ign_maskdetection of project NIVA (www.niva4cap.eu)
# All rights reserved
#
# Project and code is made available under the EU-PL v 1.2 license.
#

import json
import sys
from pathlib import Path
import cv2 as cv
import numpy as np
from osgeo import osr, ogr, gdal

# this allows GDAL to throw Python Exceptions
gdal.UseExceptions()


kernel1 = cv.getStructuringElement(cv.MORPH_ELLIPSE, (1, 1))
kernel2 = cv.getStructuringElement(cv.MORPH_ELLIPSE, (2, 2))
kernel5 = cv.getStructuringElement(cv.MORPH_ELLIPSE, (5, 5))
kernel7 = cv.getStructuringElement(cv.MORPH_ELLIPSE, (7, 7))
kernel10 = cv.getStructuringElement(cv.MORPH_ELLIPSE, (10, 10))
kernel15 = cv.getStructuringElement(cv.MORPH_ELLIPSE, (15, 15))
kernel20 = cv.getStructuringElement(cv.MORPH_ELLIPSE, (20, 20))
kernel25 = cv.getStructuringElement(cv.MORPH_ELLIPSE, (25, 25))
kernel40 = cv.getStructuringElement(cv.MORPH_ELLIPSE, (40, 40))
kernel50 = cv.getStructuringElement(cv.MORPH_ELLIPSE, (50, 50))
kernel100 = cv.getStructuringElement(cv.MORPH_ELLIPSE, (100, 100))

BAT_DETECTION_THRESHOLD = 100
VEG_DETECTION_THRESHOLD = 5
srs = osr.SpatialReference()
srs.ImportFromEPSG(2154)
wktL93 = srs.ExportToWkt()
sna_surfaces_ignore = {"SNA_IGNORE": "'ENM', 'ENA', 'MAR', 'ROC', 'CHE'"}
jacheres_surfaces_ignore = {"JACHERE_IGNORE": "'J5M', 'J6S', 'JNO'"}
category_ignore_code = {"PP_IGNORE": "'J6P', 'PRL', 'PPH', 'SPL', 'SPH', 'ROS', 'BOP', 'CAE', 'CEE'",
                        "TA_IGNORE": "'J5M', 'J6S', 'MID', 'MIE', 'MIS', 'MCT', 'TRN', 'FLA', 'CZH'",
                        "CP_IGNORE": "'MCT'",
                        "NOCAT_IGNORE": "'JNO'"}


def get_surfaces_shorter_than(img_input, max_length, max_width):
    """return all surfaces having a length and a width less than
       max_length and maw_width.

    Parameters
    ----------
    img_input : ndarray
        An 8bit single band image representing the detection mask which
        must be treated.
    max_length : maximun length of surfaces.
    max_width : maximun width of surfaces.
    Returns
         new mask.
    -------
    """
    # Empty mask.
    tmp = np.zeros(img_input.shape, np.uint8)
    contours, hierarchy = cv.findContours(img_input, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_NONE)[-2:]
    i = 0
    for contour in contours:
        if hierarchy[0, i, 3] == -1:
            rect = cv.minAreaRect(contour)
            if rect[1][0] > rect[1][1]:
                length_rect = rect[1][0]
                width_rect = rect[1][1]
            else:
                length_rect = rect[1][1]
                width_rect = rect[1][0]
            if (length_rect <= max_length and width_rect <= max_width):
                cv.drawContours(tmp, contours, i, (255, 255, 255), thickness=cv.FILLED)
        i += 1
    return tmp


def get_surfaces_longer_than(img_input, min_length, max_width):
    """return all surfaces having a length more min_length and
       width less than max_width.

    Parameters
    ----------
    img_input : ndarray
        An 8bit single band image representing the detection mask which
        must be treated.
    min_length : minimum length of surfaces.
    max_width : maximun width of surfaces.
    Returns
         new mask.
    -------
    """
    # Empty mask.
    tmp = np.zeros(img_input.shape, np.uint8)
    contours, hierarchy = cv.findContours(img_input, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_NONE)[-2:]
    i = 0
    for contour in contours:
        if hierarchy[0, i, 3] == -1:
            rect = cv.minAreaRect(contour)
            if rect[1][0] > rect[1][1]:
                length_rect = rect[1][0]
                width_rect = rect[1][1]
            else:
                length_rect = rect[1][1]
                width_rect = rect[1][0]
            if (length_rect >= min_length and width_rect <= max_width):
                cv.drawContours(tmp, contours, i, (255, 255, 255), thickness=cv.FILLED)
        i += 1
    return tmp


def get_deleted_surfaces(old_surfaces, new_surfaces, morph_op, kernel):
    """return the image of surfaces that have disappeared
       in the "new_surfaces" mask.docker run -d -p 80:80 docker/getting-started

    Parameters
    ----------
    old_surfaces: ndarray
        An 8-bit single-band image representing the mask of old surfaces state.
    img_surfaces: ndarray
        An 8-bit single-band image representing the mask of new surfaces state.
    kernel: morph open kernel apply on new mask.
    Returns
         An 8-bit single-band image representing the mask of surfaces that
          have totally disappeared.
    """
    tmp = cv.bitwise_xor(old_surfaces, cv.bitwise_and(new_surfaces, old_surfaces))
    result = cv.morphologyEx(tmp, morph_op, kernel)
    return result


def get_created_surfaces(old_surfaces, new_surfaces, morph_op, kernel):
    """return the image of surfaces that have appeared in the
       "new_surfaces" mask.

    Parameters
    ----------
    old_surfaces: ndarray
        An 8-bit single-band image representing the mask of old surfaces state.
    img_surfaces: ndarray
        An 8-bit single-band image representing the mask of new surfaces state.
    kernel: morph open kerne apply on new mask.
    Returns
         An 8-bit single-band image representing the mask of surfaces that
         have appeared.
    """
    tmp = cv.bitwise_xor(new_surfaces, cv.bitwise_and(new_surfaces, old_surfaces))
    result = cv.morphologyEx(tmp, morph_op, kernel)
    return result


def process_create_points(input_poly_src, output_pt_dst):
    """Create Points from polygon for each alert.

    Parameters
    ----------
    input_poly_src : string
        a valid ogr src
    output_pt_dst : string
        a valid ogr dst
    """
    creation_alert_poly = ogr.Open(input_poly_src, update=0)
    creation_alert_pt = ogr.Open(output_pt_dst, update=1)
    layer_creation_poly = creation_alert_poly.GetLayer(0)
    layer_creation_pt = creation_alert_pt.GetLayer(0)
    for feature in layer_creation_poly:
        point = ogr.Feature(layer_creation_pt.GetLayerDefn())
        point.SetGeometry(feature.GetGeometryRef().Centroid())
        layer_creation_pt.CreateFeature(point)


def create_tile(tile, wkt, gdal_type):
    """Create a georef image.

    Parameters
    ----------
    tile : dict
        (expected: xmin, ymin, xmax, ymax, resolution)
        The tile's parameters

    """
    black_tile_byte = gdal.GetDriverByName('MEM').Create('',
                                                         int((tile['xmax']-tile['xmin'])/tile['resolution']),
                                                         int((tile['ymax']-tile['ymin'])/tile['resolution']),
                                                         1, gdal_type)
    black_tile_byte.SetGeoTransform((tile['xmin'], tile['resolution'], 0, tile['ymax'], 0, -tile['resolution']))
    black_tile_byte.SetProjection(wkt)
    black_tile_byte.GetRasterBand(1).SetNoDataValue(0)
    return black_tile_byte


def rasterize(tile, wkt, src, *sqlStat):
    """Rasterize a vector source.

    Parameters
    ----------
    tile : dict
        (expected: xmin, ymin, xmax, ymax, resolution)
        The tile's parameters
    """
    gdal_img = create_tile(tile, wkt, gdal.GDT_Byte)
    if len(sqlStat) == 1:
        gdal.Rasterize(gdal_img, src, burnValues=255, SQLStatement=sqlStat[0])
    else:
        gdal.Rasterize(gdal_img, src, burnValues=255)
    return gdal_img.GetRasterBand(1).ReadAsArray()


def warp(tile, wkt, src):
    """Warp a raster source

    Parameters
    ----------
    tile : dict
        (expected: xmin, ymin, xmax, ymax, resolution)
        The tile's parameters

    """
    gdal_src = gdal.Open(src)
    gdal_img = create_tile(tile, wkt, gdal_src.GetRasterBand(1).DataType)
    gdal.Warp(gdal_img, gdal_src)
    return gdal_img.GetRasterBand(1).ReadAsArray()


def polygonize(tile, wkt, img, output):
    """Polygonize

    Parameters
    ----------
    tile : dict
        (expected: xmin, ymin, xmax, ymax, resolution)
        The tile's parameters

    """
    target = ogr.Open(output, update=1)
    gdal_img = create_tile(tile, wkt, gdal.GDT_Byte)
    gdal_img.GetRasterBand(1).WriteArray(img)
    gdal_img.GetRasterBand(1).FlushCache()
    layer = target.GetLayer()
    # create a "DN" field if not exist in the layer (for polygonize)
    dst_field = layer.GetLayerDefn().GetFieldIndex('DN')
    if dst_field < 0:
        layer.CreateField(ogr.FieldDefn('DN', ogr.OFTInteger))
    gdal.Polygonize(gdal_img.GetRasterBand(1), gdal_img.GetRasterBand(1), layer, 0)


def export_geotiff(tile, wkt, img, filename, gdal_type=gdal.GDT_Byte):
    """Export a GeoTiff

    Parameters
    ----------
    tile : dict
        (expected: xmin, ymin, xmax, ymax, resolution)
        The tile's parameters

    """
    gdal_img = create_tile(tile, wkt, gdal_type)
    gdal_img.GetRasterBand(1).WriteArray(img)
    gdal_img.GetRasterBand(1).FlushCache()
    gdal.GetDriverByName('GTiff').CreateCopy(filename, gdal_img, strict=0)


def process_command(paramfile, cmd, process_tile_cb):
    """?"""
    # load parameters file
    fparam = open(paramfile)
    param = json.load(fparam)

    if cmd == 'tile':
        tile = {'xmin': float(sys.argv[3]),
                'ymin': float(sys.argv[4]),
                'xmax': float(sys.argv[5]),
                'ymax': float(sys.argv[6]),
                'resolution': param['param']['Resolution']}

        process_tile_cb(param['param'], tile)

    elif cmd == 'createPt':
        process_create_points(param['param']['CreationAlert_Poly'],
                              param['param']['CreationAlert_Pt'])
        process_create_points(param['param']['DestructionAlert_Poly'],
                              param['param']['DestructionAlert_Pt'])


def get_table_name(param):
    """
    Parameters
    ----------
    param: dict
    Returns
    -------
    class name: string
    """
    result = Path(param).stem
    # it is PG string
    if result.startswith('PG:'):
        tmp = result.replace('PG:', '')
        tmp_dict = dict((x.strip().lower(), y.strip())
                        for x, y in (element.split('=') for element in tmp.split(' ')))
        result = tmp_dict['tables']
    return result


def delete_area_less_than(img_input, area_size):
    """Detect and Remove area less than "area_size" square meter.
    Parameters
    ----------
    img_input: ndarray
        An 8bit single band image representing the detection mask which
        must be treated.
    area_size: int
        areas in square meters to be removed.
    Returns
    -------
    new_img_output: ndarray
        An 8bit single band image representing the new mask without area less than
        "area_size" square meter.
    """
    kernel1_rec = cv.getStructuringElement(cv.MORPH_RECT, (1, 1))
    cv.morphologyEx(img_input, cv.MORPH_OPEN, kernel1_rec)
    cv.morphologyEx(img_input, cv.MORPH_CLOSE, kernel1_rec)

    # Empty mask.
    img_mask = np.zeros(img_input.shape, np.uint8)
    contours, hierarchy = cv.findContours(img_input, cv.RETR_CCOMP, cv.CHAIN_APPROX_NONE)[-2:]
    i = 0

    for contour in contours:
        if hierarchy[0, i, 3] == -1:
            s_contour_area = cv.contourArea(contour)
            if s_contour_area <= area_size:
                cv.drawContours(img_mask, contours, i, (255, 255, 255), thickness=cv.FILLED)
        i += 1
    new_img_output = cv.bitwise_and(img_input, cv.bitwise_not(img_mask))

    return new_img_output


def delete_area_greater_than(img_input, area_size):
    """Detect and Remove the surface larger than "area_size" square meter.
    Parameters
    ----------
    img_input: ndarray
        An 8bit single band image representing the detection mask which
        must be treated.
    area_size: int
        areas in square meters to be removed.
    Returns
    -------
    new_img_output: ndarray
        An 8bit single band image representing the new mask without area less than
        "area_size" square meter.
    """
    kernel1_rec = cv.getStructuringElement(cv.MORPH_RECT, (1, 1))
    cv.morphologyEx(img_input, cv.MORPH_OPEN, kernel1_rec)
    cv.morphologyEx(img_input, cv.MORPH_CLOSE, kernel1_rec)

    # Empty mask.
    img_mask = np.zeros(img_input.shape, np.uint8)
    contours, hierarchy = cv.findContours(img_input, cv.RETR_CCOMP, cv.CHAIN_APPROX_NONE)[-2:]
    i = 0

    for contour in contours:
        if hierarchy[0, i, 3] == -1:
            s_contour_area = cv.contourArea(contour)
            if s_contour_area >= area_size:
                cv.drawContours(img_mask, contours, i, (255, 255, 255), thickness=cv.FILLED)
        i += 1

    new_img_output = cv.bitwise_and(img_input, cv.bitwise_not(img_mask))

    return new_img_output


def detect_forest_on_parcels(img_vegetation, img_parcels, img_sna):
    """Detect forests that spill over parcels, counted as new Ecological Focus Areas.
    Parameters
    ----------
    img_vegetation: ndarray
        An 8bit single band image representing a detection mask of forests.
    img_parcels: ndarray
        An 8bit single band image image representing a mask of parcels.
    img_sna: ndarray
        An 8bit single band image image representing a mask of Ecological Focus Areas.
    Returns
    -------
    img_new_efa: ndarray
        An 8bit single band image image representing a mask of new forests (or EFAs).
    """

    # Erode of 15m.
    tmp = cv.morphologyEx(img_vegetation, cv.MORPH_CLOSE, kernel15)
    tmp = cv.morphologyEx(tmp, cv.MORPH_ERODE, kernel15)

    # Intersect with parcels to keep only what's outside parcels.
    tmp = cv.bitwise_and(tmp, cv.bitwise_not(img_parcels))
    tmp = cv.morphologyEx(tmp, cv.MORPH_OPEN, kernel5)

    # Dilate forests of 40m (include some margin not to have zones too close to forests).
    tmp = cv.morphologyEx(tmp, cv.MORPH_DILATE, kernel40)

    # Delete those forests from initial detection, keep only what's in parcels.
    tmp = cv.bitwise_and(img_vegetation, cv.bitwise_not(tmp))

    # Delete what's outside parcels.
    tmp = cv.bitwise_and(tmp, img_parcels)

    # Delete what's too close to existing SNA.
    tmp_sna = cv.morphologyEx(img_sna, cv.MORPH_DILATE, kernel15)
    tmp = cv.bitwise_and(tmp, cv.bitwise_not(tmp_sna))

    # Delete small area near parcel borders.
    inside_parcels = cv.morphologyEx(img_parcels, cv.MORPH_ERODE, kernel10)
    detect_border = cv.bitwise_and(tmp, cv.bitwise_not(inside_parcels))
    detect_inside = cv.bitwise_and(tmp, inside_parcels)

    # Delete small areas in borders.
    img_new_sna_borders = cv.morphologyEx(detect_border, cv.MORPH_OPEN, kernel7)

    # Delete very small areas in inside.
    img_new_sna_inside = cv.morphologyEx(detect_inside, cv.MORPH_OPEN, kernel2)

    # Merge inside and borders.
    img_new_sna = cv.bitwise_or(img_new_sna_borders, img_new_sna_inside)

    return img_new_sna


def get_deleted_sna(img_sna, img_vegetation, img_parcels):
    """ Test if the intersection of the sna and the parcels on the database
    still contains building or the vegetation. Otherwise the 'sna's have disappeared.
    Parameters
    ----------
    img_sna: ndarray
        An 8bit single band image representing a detection mask of sna.
    img_vegetation: ndarray
        An 8bit single band image representing a mask of vegetation.
    img_parcels: ndarray
        An 8bit single band image representing a mask of parcels.
    Returns
    -------
    img_new_sna: ndarray
        An 8bit single band image representing a mask sna destruction .
    """

    # Empty mask.
    img_mask = np.zeros(img_sna.shape, np.uint8)

    d_img_parcels = cv.morphologyEx(img_parcels, cv.MORPH_ERODE, kernel10)
    tmp = cv.bitwise_and(img_sna, d_img_parcels)
    img_sna_in_parcels = cv.morphologyEx(tmp, cv.MORPH_OPEN, kernel2)

    # Intersect with sna mask.
    img_intersect_sna_in_parcels = cv.bitwise_and(img_sna_in_parcels, img_vegetation)

    contours, _ = cv.findContours(img_sna_in_parcels, cv.RETR_EXTERNAL, cv.CHAIN_APPROX_NONE)[-2:]
    i = 0
    for contour in contours:
        current_area = cv.contourArea(contour)
        if current_area > 50:
            [x_contour, y_contour, w_contour, h_contour] = cv.boundingRect(contour)
            crop_img_intersect_sna_in_parcels = img_intersect_sna_in_parcels[y_contour:y_contour+h_contour,
                                                                             x_contour:x_contour+w_contour]
            # Count pixels not zero.
            num_pixels = cv.countNonZero(crop_img_intersect_sna_in_parcels)
            # Contour sna contains no veget ==> alert it.
            if num_pixels == 0:
                cv.drawContours(img_mask, contours, i, (255, 255, 255), thickness=cv.FILLED)
        i += 1
    return img_mask


def return_alerts_destruction_sna(param,
                                  tile,
                                  img_parcels,
                                  img_building_mask,
                                  img_vegetation_mask):
    """Return a mask of sna surfaces that have disappeared.
    Parameters
    ----------
    param: dict
        Parameters for the process.
    tile: dict
        (expected: xmin, ymin, xmax, ymax, resolution).
        The tile's bbox.
    img_parcels: ndarray
        An 8bit single band image representing the parcels.
    img_building_mask: ndarray
        An 8bit single band image representing the buildings.
    img_vegetation_mask: ndarray
        An 8bit single band image representing the vegetations.
    Returns
    -------
    img_destruction: ndarray
        An 8bit single band image representing the sna destructions.
    """

    # Rasterize non agricultural surfaces,
    # sna.FOR, sna.HAI, sna.BRO, sna.BOS
    # only used to find destruction veg in a first time.
    img_sna_selected = rasterize(tile,
                                 wktL93,
                                 param['SNA'],
                                 "select * from "
                                 + get_table_name(param['SNA']) +
                                 " WHERE trigramme IN (\
                                 'FOR','HAI','BRO','BOS','ARA')")

    # Rasterize SNA.bat.
    img_old_building = rasterize(tile,
                                 wktL93,
                                 param['SNA'],
                                 "select * from "
                                 + get_table_name(param['SNA']) +
                                 " where trigramme IN (\
                                 'BAT')")

    # ----------------------------------------
    # DESTRUCTION sna.BAT.

    d_img_building_mask = cv.morphologyEx(img_building_mask,
                                          cv.MORPH_DILATE,
                                          kernel5)

    tmp = get_deleted_surfaces(img_old_building,
                               d_img_building_mask,
                               cv.MORPH_OPEN,
                               kernel5)
    # Create buffer (20 meters) around parcels.
    d_img_parcels = cv.morphologyEx(img_parcels, cv.MORPH_DILATE, kernel40)

    # Rasterize sna.SAM.
    img_sna_sam = rasterize(tile,
                            wktL93,
                            param['SNA'],
                            "select * from "
                            + get_table_name(param['SNA']) +
                            " where trigramme IN (\
                            'SAM')")

    # Keep destruction only on dilated parcels, sna.SAM exclude.
    img_destruction_bat = cv.bitwise_and(tmp,
                                         cv.bitwise_and(d_img_parcels,
                                                        cv.bitwise_not(img_sna_sam)))

    # END DESTRUCTION sna.BAT.
    # ----------------------------------------

    img_destruction_veg = get_deleted_sna(img_sna_selected,
                                          img_vegetation_mask,
                                          img_parcels)

    # Select PP_IGNORE on Parcels.
    img_pp_ignore = rasterize(tile,
                              wktL93,
                              param['Parcels'],
                              "select * from "
                              + get_table_name(param['Parcels']) +
                              " where code_cultu IN ("
                              + category_ignore_code["PP_IGNORE"] +
                              ")")

    # Select Jacheres on Parcels .
    ignore_jachere_veg = rasterize(tile,
                                   wktL93,
                                   param['Parcels'],
                                   "select * from "
                                   + get_table_name(param['Parcels']) +
                                   " where code_cultu IN ("
                                   + jacheres_surfaces_ignore["JACHERE_IGNORE"] +
                                   ")")

    # Select sna.FOR, BRO, BOS, overlap PP parcels.
    img_for_bro_bos = rasterize(tile,
                                wktL93,
                                param['SNA'],
                                "select * from "
                                + get_table_name(param['SNA']) +
                                " WHERE trigramme IN (\
                                'FOR','BRO','BOS')")

    keep_alerts_pp = cv.bitwise_and(img_pp_ignore, img_for_bro_bos)
    # Keep alerts destruction for this.
    tmp = cv.bitwise_and(img_destruction_veg, keep_alerts_pp)
    # Filtered selected alerts, delete area less than 5000 square meters.
    keep_alerts_pp = delete_area_less_than(tmp, 5000)

    # Select sna.BRO.
    tmp = rasterize(tile,
                    wktL93,
                    param['SNA'],
                    "select * from "
                    + get_table_name(param['SNA']) +
                    " WHERE trigramme IN (\
                    'BRO')")
    # Select ALL alerts destructions for sna.BRO.
    exclude_bro_mask = cv.bitwise_and(img_destruction_veg, tmp)
    # Create mask : Delete all new area greater than 100 square meters.
    tmp = delete_area_greater_than(exclude_bro_mask, 100)
    exclude_bro_mask = cv.morphologyEx(tmp, cv.MORPH_DILATE, kernel10)

    # Select sna.HAI.
    img_hai_ara = rasterize(tile,
                            wktL93,
                            param['SNA'],
                            "select * from "
                            + get_table_name(param['SNA']) +
                            " where trigramme IN (\
                            'HAI', 'ARA')")
    # Select alerts destructions for sna.HAI sna.ARA only on PP IGNORE parcels.
    tmp = cv.bitwise_and(img_destruction_veg,
                         cv.bitwise_and(img_pp_ignore,
                                        img_hai_ara))
    # Filtered sna.HAI, sna.ARA selected (minimum length 50, max width 10 meters).
    keep_alerts_hai_ara = get_surfaces_longer_than(tmp, 50, 10)

    # Clean all alerts destruction on PP_IGNORE.
    tmp = cv.bitwise_and(img_destruction_veg, cv.bitwise_not(img_pp_ignore))

    # Clean all alerts destruction on JACHERE_IGNORE.
    img_destruction_veg = cv.bitwise_and(tmp, cv.bitwise_not(ignore_jachere_veg))

    # Delete all sna.BRO alerts if area < 100 square meters.(Nothing append in PP parcels).
    tmp = cv.bitwise_and(img_destruction_veg, cv.bitwise_not(exclude_bro_mask))

    # Re added alerts destruction sna.FOR, sna.BRO, sna.BOS on PP if area > 5000 square meters.
    img_destruction_veg = cv.bitwise_or(tmp, keep_alerts_pp)
    # Re added alerts destruction on HAI if length> 50 meters.
    tmp = cv.bitwise_or(img_destruction_veg, keep_alerts_hai_ara)
    img_destruction_veg = tmp

    # Now merge img_destruction_bat and img_destruction_veg.
    tmp = cv.bitwise_or(img_destruction_veg, img_destruction_bat)

    # Ignore alerts on this sna.
    sna_ignore = rasterize(tile,
                           wktL93,
                           param['SNA'],
                           "select * from "
                           + get_table_name(param['SNA']) +
                           " where trigramme IN ("
                           + sna_surfaces_ignore["SNA_IGNORE"] +
                           ")")
    img_destruction = cv.bitwise_and(tmp, cv.bitwise_not(sna_ignore))

    return img_destruction


def return_alerts_creation_sna(param,
                               tile,
                               img_parcels,
                               img_building_mask,
                               img_vegetation_mask):
    """Return a mask of sna surfaces that have appeared.
    Parameters
    ----------
    param: dict
        Parameters for the process.
    tile: dict
        (expected: xmin, ymin, xmax, ymax, resolution).
        The tile's bbox.
    img_parcels: ndarray
        An 8bit single band image representing the parcels.
    img_building_mask: ndarray
        An 8bit single band image representing the buildings.
    img_vegetation_mask: ndarray
        An 8bit single band image representing the vegetations.
    Returns
    -------
    img_creation: ndarray
        An 8bit single band image representing the sna creations.
    """
    # Rasterize SNA.bat.
    img_old_building = rasterize(tile,
                                 wktL93,
                                 param['SNA'],
                                 "select * from "
                                 + get_table_name(param['SNA']) +
                                 " where trigramme IN (\
                                 'BAT')")

    ignore_parcels_bat = rasterize(tile,
                                   wktL93,
                                   param['Parcels'],
                                   "select * from "
                                   + get_table_name(param['Parcels']) +
                                   " WHERE code_cultu IN ("
                                   + category_ignore_code["TA_IGNORE"] +
                                   ", "
                                   + category_ignore_code["CP_IGNORE"] +
                                   ")")

    # Rasterize non agricultural surfaces,
    # SNA.FOR, SNA.SAM, SNA.HAI, SNA.BRO, SNA.BOS used to find creation.
    img_sna_selected = rasterize(tile,
                                 wktL93,
                                 param['SNA'],
                                 "select * from "
                                 + get_table_name(param['SNA']) +
                                 " where trigramme IN (\
                                 'FOR','SAM','HAI','BRO','BOS')")

    # ----------------------------------------
    # BEGIN CREATION sna.BAT.
    tmp = get_created_surfaces(img_old_building,
                               img_building_mask,
                               cv.MORPH_OPEN,
                               kernel5)
    # Create buffer (20 meters) around parcels.
    d_img_parcels = cv.morphologyEx(img_parcels, cv.MORPH_DILATE, kernel40)

    # Rasterize sna.SAM.
    img_sna_sam = rasterize(tile,
                            wktL93,
                            param['SNA'],
                            "select * from "
                            + get_table_name(param['SNA']) +
                            " where trigramme IN (\
                            'SAM')")

    # Keep creation only on dilated parcels, sna.SAM exclude.
    img_creation_bat = cv.bitwise_and(tmp,
                                      cv.bitwise_and(d_img_parcels,
                                                     cv.bitwise_not(img_sna_sam)))

    # Exclude alerts creation bat in TA and CP (over detection bati).
    tmp = cv.bitwise_and(img_creation_bat, cv.bitwise_not(ignore_parcels_bat))

    # Elimitate small areas.
    img_creation_bat = cv.morphologyEx(tmp, cv.MORPH_OPEN, kernel5)

    # END CREATION sna.BAT.
    # ----------------------------------------

    # Detect forests that spill over parcels.
    img_creation_veg = detect_forest_on_parcels(img_vegetation_mask, img_parcels, img_sna_selected)

    # Exclude mask, delete all alerts on SNA.ARB.
    tmp = rasterize(tile,
                    wktL93,
                    param['SNA'],
                    "select * from "
                    + get_table_name(param['SNA']) +
                    " where trigramme IN (\
                    'ARB')")
    ignore_arb_mask = cv.morphologyEx(tmp, cv.MORPH_DILATE, kernel10)

    # Select on Parcels PP_IGNORE, TA_IGNORE, CP_IGNORE.
    ignore_parcels_veg = rasterize(tile,
                                   wktL93,
                                   param['Parcels'],
                                   "select * from "
                                   + get_table_name(param['Parcels']) +
                                   " where code_cultu IN ("
                                   + category_ignore_code["PP_IGNORE"] +
                                   ", "
                                   + category_ignore_code["CP_IGNORE"] +
                                   ", "
                                   + category_ignore_code["TA_IGNORE"] +
                                   ","
                                   + category_ignore_code["NOCAT_IGNORE"] +
                                   ")")

    # Apply ignore_parcels_veg on creation mask.
    tmp = cv.bitwise_and(img_creation_veg, cv.bitwise_not(ignore_parcels_veg))

    img_creation_veg = tmp

    # Clean all alerts on sna.ARB.
    tmp = cv.bitwise_and(img_creation_veg, cv.bitwise_not(ignore_arb_mask))
    img_creation_veg = cv.morphologyEx(tmp, cv.MORPH_OPEN, kernel10)

    # Now merge img_destruction_bat and img_destruction_veg.
    tmp = cv.bitwise_or(img_creation_bat, img_creation_veg)

    # Ignore alerts on this sna.
    sna_ignore = rasterize(tile,
                           wktL93,
                           param['SNA'],
                           "select * from "
                           + get_table_name(param['SNA']) +
                           " where trigramme IN ("
                           + sna_surfaces_ignore["SNA_IGNORE"] +
                           ")")
    img_creation = cv.bitwise_and(tmp, cv.bitwise_not(sna_ignore))

    return img_creation


def process_tile(param, tile):
    """Process a detection mask of forests to deduce new SNAs on parcels.
    Parameters
    ----------
    param: dict
        Parameters for the process
        (expected: CreationAlertPoly, Parcels, EFA, Vegetation).
    tile: dict
        (expected: xmin, ymin, xmax, ymax, resolution)
        The tile's bbox.
    """
    # Rasterize parcels.
    img_parcels = rasterize(tile, wktL93, param['Parcels'])

    # Building mask.
    tmp = warp(tile, wktL93, param['New_Building'])
    _, img_building_mask = cv.threshold(tmp, BAT_DETECTION_THRESHOLD, 255., cv.THRESH_BINARY)

    # Vegetation mask.
    tmp = warp(tile, wktL93, param['Veg_Mask'])
    _, img_vegetation_mask = cv.threshold(tmp, VEG_DETECTION_THRESHOLD, 255., cv.THRESH_BINARY)

    # Rasterize Electric_Line.
    tmp = rasterize(tile, wktL93, param['Electric_Lines'])
    img_electric_lines = cv.morphologyEx(tmp, cv.MORPH_DILATE, kernel50)

    img_destruction = return_alerts_destruction_sna(param,
                                                    tile,
                                                    img_parcels,
                                                    img_building_mask,
                                                    img_vegetation_mask)

    img_creation = return_alerts_creation_sna(param,
                                              tile,
                                              img_parcels,
                                              img_building_mask,
                                              img_vegetation_mask)

    # Elimitate alerts destruction under electric lines.
    tmp = cv.bitwise_and(img_destruction, cv.bitwise_not(img_electric_lines))
    # And clean.
    img_destruction = cv.morphologyEx(tmp, cv.MORPH_OPEN, kernel1)

    # Eliminate alerts creation under electric lines.
    tmp = cv.bitwise_and(img_creation, cv.bitwise_not(img_electric_lines))
    # And clean.
    img_creation = cv.morphologyEx(tmp, cv.MORPH_OPEN, kernel1)

    # Used Exclusion zone.
    if param['Used_ExcludeZone']:
        img_zone_exclusion = rasterize(tile, wktL93, param['ExcludeZone'])

        tmp = cv.bitwise_and(img_destruction, cv.bitwise_not(img_zone_exclusion))
        d_img_destruction = cv.morphologyEx(tmp, cv.MORPH_DILATE, kernel100)
        # d_img_destruction = tmp

        tmp = cv.bitwise_and(img_creation, cv.bitwise_not(img_zone_exclusion))
        d_img_creation = cv.morphologyEx(tmp, cv.MORPH_DILATE, kernel100)
        # d_img_creation = tmp

    else:
        # Dilate mask for grouping alerts.
        d_img_destruction = cv.morphologyEx(img_destruction, cv.MORPH_DILATE, kernel100)
        d_img_creation = cv.morphologyEx(img_creation, cv.MORPH_DILATE, kernel100)
        # d_img_destruction = img_destruction
        # d_img_creation = img_creation

    # Polygonize result.
    polygonize(tile, wktL93, d_img_destruction, param['DestructionAlert_Poly'])
    polygonize(tile, wktL93, d_img_creation, param['CreationAlert_Poly'])


if __name__ == "__main__":
    if len(sys.argv) == 1:
        print("No parameters defined")
        print("Usage: python alerts_from_veg.py parameters.json command xmin ymin xmax ymax")
        sys.exit(1)

    param_file = sys.argv[1]
    command = sys.argv[2]

    process_command(param_file, command, process_tile)
