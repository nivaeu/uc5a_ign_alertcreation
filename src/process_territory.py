"""This script launch tiled process on a territory"""
#
# Copyright (c) IGN 2019 -- 2021.
# This file belongs to subproject uc5a_ign_maskdetection of project NIVA (www.niva4cap.eu)
# All rights reserved
#
# Project and code is made available under the EU-PL v 1.2 license.
#

import sys
import os
import csv

if __name__ == "__main__":

    if len(sys.argv) == 1:
        print("No parameters defined")
        print("Usage : python PROCESS_DEP.py <script_used> <paramaters_file> <command> <dep_bbox_file>.")
        print(" <script_used>     : python script file used for processing department (ex: alerts.py).")
        print(" <paramaters_file> : configuration file (ex: params_veg_PUYDOME.json).")
        print(" <command>         : possible values tile or createPt or all.")
        print(" <dep_bbox_file>   : department bbox (ex: EMP_CHANTIER_PUYDOME.csv).")
        sys.exit(1)
    else:
        script_used = sys.argv[1]
        file_param = sys.argv[2]
        command = sys.argv[3]
        emprise_file = sys.argv[4]

        if command not in {'tile', 'createPt', 'all'}:
            print("command ["+command+"] unknown.. \'tile\' or \'createPt\' or \'all\' expected.")
            print("Process ended with errors.")
            sys.exit(1)
        if command in {'tile', 'all'}:
            with open(emprise_file, newline='\n') as csvfile:
                spamreader = csv.DictReader(csvfile)
                print(spamreader.line_num)
                for row in spamreader:
                    Xmin = (int)(row['Xmin'])
                    Ymin = (int)(row['Ymin'])
                    Xmax = (int)(row['Xmax'])
                    Ymax = (int)(row['Ymax'])
                    Xstep = (int)(row['Xstep'])
                    Ystep = (int)(row['Ystep'])
                    Y = Ymin
                    while Y < Ymax:
                        X = Xmin
                        while X < Xmax:
                            t1 = X+Xstep
                            t2 = Y+Ystep
                            proc = ("python "+script_used+" "+file_param+" " +
                                    "tile"+" "+str(X)+" "+str(Y)+" "+str(X+Xstep)+" "+str(Y+Ystep))
                            print("->"+proc)
                            os.system(proc)
                            X += Xstep
                        Y += Ystep
        if command in {'createPt', 'all'}:
            print("Création des ponctuels à partir des alertes poly")
            CMD = "createPt"
            proc = ("python "+script_used+" "+file_param+" "+CMD)
            print("->"+proc)
            os.system(proc)
        print("Process ended!")
