# UC5A_IGN_ALERTCREATION

## Introduction
This sub-project is part of the ["New IACS Vision in Action”
--- NIVA](https://www.niva4cap.eu/) project that delivers a
a suite of digital solutions, e-tools and good practices for
e-governance and initiates an innovation ecosystem to
support further development of IACS that will facilitate
data and information flows.
This project has received funding from the European Union’s
Horizon 2020 research and innovation programme under
grant agreement No 842009.
Please visit the [website](https://www.niva4cap.eu) for
further information. A complete list of the sub-projects
made available under the NIVA project can be found on
[gitlab](https://gitlab.com/nivaeu/)

## Presentation

This project contains python scripts to produce change detection alerts. It is based on comparaisons between deep learning masks (one with vegetation and one with buildings) and the database.

## Deep learning mask preparation

The two deep learning masks are produced with an open-source project based on PyToch (being published).
Input data: RGB Ortho Image + IR (near Infra Red) + DSM (digital Surface Model)

## Alert creation

There are 3 examples of dataset in folders: data/test1, data/test2 and data/test3.
Each folder contains:

- Input rasters:
 - Ortho_XXX_YYY.tif : RGB Ortho image on the area

- Deep learning masks:
 - bati_XXX_YYY_1m.tif : building mask
 - veg_XXX_YYY_1m.tif : vegetation mask

- Input Geopackage:
  - parcelles.gpkg : agricultural parcels
  - sna.gpkg : uncultivated area
  - ligne_electrique.gpkg  : electric lines

- Output Geopackage:
  - CreationAlert_Poly.gpkg : surfacique alert for new uncultivated area
  - DestructionAlert_Poly.gpkg : surfacique alert for uncultivated area disparition

## Methodology

to do...
