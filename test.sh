#
# Copyright (c) IGN 2019 -- 2021.
# This file belongs to subproject uc5a_ign_maskdetection of project NIVA (www.niva4cap.eu)
# All rights reserved
#
# Project and code is made available under the EU-PL v 1.2 license.
#
echo Test1
cd data/test1 && sh clean.sh && sh runTEST1.sh && cd ../..
echo Test2
cd data/test2 && sh clean.sh && sh runTEST2.sh && cd ../..
echo Test3
cd data/test3 && sh clean.sh && sh runTEST3.sh && cd ../..
